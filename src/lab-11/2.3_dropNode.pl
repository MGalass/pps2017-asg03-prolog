:-consult('1.4_dropAll.pl').

% dropNode(+Graph, +Node, -OutGraph)
% Drop all edges starting and leaving from a Node
% dropNode([e(1,2),e(1,3),e(2,3)],1,[e(2,3)]).
% NB: changed elements orders from the lab version
dropNode(G,N,O):-
	dropAll(e(N,_),G,G2),	% drop all the node starting from N, generating G2
	dropAll(e(_,N),G2,O).	% drop all the nodes in G2 ending with N