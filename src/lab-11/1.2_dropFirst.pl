:-consult('1.1_dropAny.pl').

% dropFirst(?Elem,?List,?OutList)
% Drops the first occurrence of Elem in List and returns the new list in OutList
% dropFirst(10,[10,20,10,30,10],L).
dropFirst(X,[X|T],T):- !.
dropFirst(Elem,List,OutList):- dropAny(Elem, List, OutList), !.