% dropAll(?Elem,?List,?OutList)
% Drops all the occurrences of Elem in List and returns the new list in OutList
dropAll(X,[],[]) :- !.			  
dropAll(X,[XCOPY|T],L) :- 
	copy_term(X,XCOPY),
	dropAll(X,T,L),!.	           % Deleting the head if its equal to Elem
dropAll(X,[H|T],[H|L]) :- dropAll(X,T,L).  % If not equal, we recursively act upon the tail