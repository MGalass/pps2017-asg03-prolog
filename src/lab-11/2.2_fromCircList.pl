% fromCircList(+List,-Graph)
% Obtain a graph from a circular list
% fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
% fromCircList([10,20],[e(10,20),e(20,10)]).
% fromCircList([10],[e(10,10)]).
fromCircList([],[]).
%fromCircList([X],[e(X,X)]):-!. % We don't need this anymore thanks to the last clause
fromCircList([H|T],L):-fromCircList(H,[H|T],L).	% Using a fromCircList/3 to keep track of the head

fromCircList(H,[T],[e(T,H)]).		% Connecting the tail with the head
fromCircList(H,[H1,H2|T],[e(H1,H2)|L]):-fromCircList(H,[H2|T],L).