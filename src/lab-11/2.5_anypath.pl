% anypath(+Graph, +Node1, +Node2, -ListPath)
% A path from Node1 to Node2. Iff there are many path, they are showed one by one
% anypath([e(1,2),e(1,3),e(2,3)],1,3,L). -> L/[e(1,2),e(2,3)], L/[e(1,3)]
anypath([], N1, N2, []).				% Empty graph
anypath(G, N1, N2, [e(N1,N2)]):-member(e(N1,N2),G).	% N1 and N2 are consecutive
anypath(G, N1, N2, [e(N1,X)|L]):-			
	member(e(N1,X),G),				% Putting in X all the consecutive N1 elements, one by one
	anypath(G,X,N2,L).				% For every X we recursively call anypath on it
							% Then we unify L and add the new pair (N1,X)