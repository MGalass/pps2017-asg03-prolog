% dropAny(?Elem,?List,?OutList)
% Drops any occurrence of Elem in List and returns the new list in OutList
% dropAny(10,[10,20,10,30,10],L). - 3 occurrences
% L/[20,10,30,10]
% L/[10,20,30,10]
% L/[10,20,10,30]
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).