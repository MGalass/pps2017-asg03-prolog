% Model: as a list of couples [e(1,1),e(1,2),e(2,3),e(3,1)]
% fromList(+List,-Graph)
% Obtain a graph from a list
% fromList([10,20,30],[e(10,20),e(20,30)]).
% fromList([10,20],[e(10,20)]).
% fromList([10],[]).
fromList([],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).