:-consult('2.5_anypath.pl').

% allreaching(+Graph, +Node, -List)
% All the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
% allreaching([e(1,2),e(2,3),e(3,5)],1,[2,3,5]).
allreaching([], N, []).
allreaching(G, N, L):-findall(X,anypath(G,N,X,_),L).	% Compact way to call anypath to collect every path from N