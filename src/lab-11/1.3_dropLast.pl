:-consult('1.1_dropAny.pl').
:-consult('../lab-10/4.4_inv.pl').

% dropLast(?Elem,?List,?OutList)
% Drops the last occurrence of Elem in List and returns the new list in OutList
% dropLast(10,[10,20,10,30,10],L).
dropLast(X,L,OL):-
	inv(L, IL),		% Inverting the list
	dropAny(X, IL, IOL),!,	% Dropping the first occurrence
	inv(IOL,OL),!.		% Reinverting the list and stop