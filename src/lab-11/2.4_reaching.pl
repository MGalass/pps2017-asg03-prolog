% reaching(+Graph, +Node, -List)
% Show all the nodes that can be reached in one step from Node
% reaching([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2,3]
% reaching([e(1,2),e(1,2),e(2,3)],1,L). -> L/[2,2]
reaching(G,N,L):- 
	findall(X,member(e(N,X),G),L1),	% We apply member first, looking for all the element in G which can be represented by e(N,X). We then collect all the X and put them in L
	findall(Y,member(e(Y,N),G),L2),	% We do the same for the other template
	append(L1,L2,L).		% We append the results
	
% Another version with map
% map(List,Elem,OutElem,OutList)
map(L,E,OE,OL):-findall(OE,member(E,L),OL).
reaching2(G,N,L):-
	map(G,e(N,X),X,L1),		% e(N,X) -> X
	map(G,e(Y,N),Y,L2),		% e(Y,N) -> Y
	append(L1,L2,L).