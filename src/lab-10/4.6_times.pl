:-consult('bigger.pl').

% times(@List,@N,@List2)
% Checks if List2 is N times List1
% times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(L,0,[]).
times(L1, N, L2):-
	N > 0,
	N2 is N-1,
	append(L1, Res, L2),	% Res = L2 - L1, so it should be equals to L1 * N-1
	times(L1, N2, Res),!.
	
% times_rel(@List,@N,@List2)
% Checks if List2 is N times List1 in a fully relational way
% times_rel([zero,s(zero)],s(s(zero)),[zero,s(zero),zero,s(zero)]).
times_rel(L,zero,[]).
times_rel(L1, s(N), L2):-
	bigger(s(N),zero,s(N)),
	append(L1, Res, L2),	% Res = L2 - L1, so it should be equals to L1 * N-1
	times_rel(L1, N, Res),!.
	