% Fully relation solution, using 'zero' and s(N) to represent the successor of N
% size([a,b,c],X). -> X/s(s(s(zero)))
% size(L, s(s(s(zero)))). -> Generating a random list with exactly s(s(s(zero)) elements (3)
size([],zero).
size([_|T],s(N)) :- size(T,N).