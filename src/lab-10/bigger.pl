% Fully relational version needs "bigger" predicate
% bigger(+X,+Y,-Z)
% Sets Z equals to the bigger between X and Y
% Parameters needs to be in Peano's form
% bigger(s(s(s(zero))), zero,X). -> X/s(s(s(zero)))
% bigger(zero,s(s(s(zero))),X). -> X/s(s(s(zero)))
% bigger(s(s(s(s(s(zero))))), s(s(s(s(s(s(zero)))))),X). X/s(s(s(s(s(s(zero))))))
bigger(X,zero,X).
bigger(zero,Y,Y).
bigger(s(X),s(Y),s(Z)):-bigger(X,Y,Z).