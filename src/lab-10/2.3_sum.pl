:-consult('plus.pl').

% sum(+List, -Sum)
% Sum will contain the sum of elements in List
% sum([1,2,3],X). -> X/6
sum([], 0).
sum([H|T], Y):-sum(T,X), Y is X + H.

% sum_rel(+List, -Sum)
% Performs X plus Y and sets the result in Z in a fully relation way
% sum_rel([s(zero),s(s(zero)),s(zero)],X). -> X / s(s(s(s(zero))))
sum_rel([], zero).
sum_rel([H|T], Y):-sum_rel(T,X), plus(X, H, Y).