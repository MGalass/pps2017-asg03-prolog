:-consult('bigger.pl').

% seqR(@N,@List)
% Given N, checks if List contains all the elements from N to 0, in order
% seqR(4,[4,3,2,1,0]).
seqR(0,[0]):-!.			% Base case
seqR(N,[N|T]):- 
	N > 0,!,
	N2 is N-1,!,
	seqR(N2,T).
	
% seqR_rel(@N,@List)
% Fully implementation of seqR(@N,@List), where numbers are represented in Peano's form
% seqR_rel(s(s(s(s(zero)))),[s(s(s(s(zero)))),s(s(s(zero))),s(s(zero)),s(zero),zero]). (4,[4,3,2,1,0])
seqR_rel(zero,[zero]):-!.			
seqR_rel(s(N),[s(N)|T]):- 		% the "successor" relation is done with s(...)
	bigger(s(N),zero,s(N)),!,	% bigger predicate
	seqR_rel(N,T).