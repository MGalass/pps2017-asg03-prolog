% proj(+List1,-List2)
% Given List1 containing pairs, generate List2 as a projection on the first element of the pairs
% proj([[1,2],[3,4],[5,6]],[1,3,5]).
% proj([[zero,s(zero)],[s(s(zero)),s(s(s(zero)))]],[zero,s(s(zero))]).
proj([],[]).
proj([H|T], [X|Xs]):-
	HFirst = X,			% Checking the equivalence
	append([HFirst], [HLast], H),	% Retriving the first element of the pair
	proj(T, Xs).