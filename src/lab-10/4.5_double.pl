% double(@List,@List)
% Checks if List2 is the double of List1
% double([1,2,3],[1,2,3,1,2,3]).
% double([zero,s(zero),s(s(zero))],[zero,s(zero),s(s(zero)),zero,s(zero),s(s(zero))]).
double([], []).
double(L1, L2):- append(L1, L1, L2).	% We append the first list to itself