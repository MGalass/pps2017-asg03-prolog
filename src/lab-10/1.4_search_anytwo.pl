:-consult('1.1_search.pl').

% search_anytwo(Elem,List)
% These clauses look for any Elem that occurs two times in List
% search_anytwo(a,[b,c,a,a,d,e]).
% search_anytwo(a,[b,c,a,d,e,a,d,e]).
search_anytwo(X, [X|Xs]):-search(X, Xs).        % If Elem is in the head we just need another occurrence
search_anytwo(X, [_|Xs]):-search_anytwo(X, Xs).
% If we want only one result and then stop, we should put a ! at the end of the first clause
% search_anytwo(X, [X|Xs]):-search(X, Xs), !. 