% search_two(@Elem, @List)
% These clauses look for two occurrences of Elem in List with an element in between
% search_two(a,[b,c,a,a,d,e]). -> no
% search_two(a,[b,c,a,d,a,d,e]). -> yes
search_two(X, [X, Y, X|_]).
search_two(X, [_|Xs]):-search_two(X,Xs).
% If we want only one result and then stop, we should put a ! at the end of the second clause
% search_two(X, [_|Xs]):-search_two(X,Xs),!.