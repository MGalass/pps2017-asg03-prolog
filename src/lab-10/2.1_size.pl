% size(+List,-Size)
% Size will contain the number of elements in List
% size([a,b,c],X). -> X/3
size([],0).
size([_|T],M) :- size(T,N), M is N+1.