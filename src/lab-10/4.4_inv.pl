:-consult('last.pl').

% inv(@List1,@List2)
% Checks if List1 is the inverse of List2 or generate one of them given the other
% inv([1,2,3],[3,2,1]).
% inv([zero, s(zero), s(s(zero))],[s(s(zero)), s(zero), zero]).
inv([],[]).
inv([X|Xs], List2):-
	X = YLast,
	last(Y2, YLast, List2),	% Decomposing List2
	inv(Xs, Y2),!.