% search(@Elem, @List)
% These clauses let us:
% 1) Check if List contains Elem
% 2) Iterate the list, one element after the other
% 3) Generate a new List (containing Elem or not)
search(X, [X|_]).
search(X, [_|Xs]):-search(X,Xs).