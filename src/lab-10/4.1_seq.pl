:-consult('bigger.pl').

% seq(@N,@List)
% Given N, checks if List contains exactly N elements equals to 0
% seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):-N > 0, N2 is N-1, seq(N2,T).

% seq_rel(@N, @List)
% Checks if List contains exactly N "zero" elements
% Fully functional version, using Peano's form
% seq_rel(s(s(s(s(s(zero))))),[zero, zero, zero, zero, zero]).
% seq_rel(s(s(zero)),X). -> X / [zero,zero]  Generating the list
seq_rel(zero,[]).
seq_rel(s(N),[zero|T]):- bigger(s(N),zero,s(N)), seq_rel(N,T).