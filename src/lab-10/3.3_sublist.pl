:-consult('../lab-11/1.1_dropAny.pl').

% sublist(List1,List2)
% List1 should be a subset of List2
% sublist([1,2],[5,3,2,1]).
% This version works for the selected example, but fails in an element is repeated.
% This is because List2 doesn't drop the searched element.
% sublist([],[]).		% Both are empty
% sublist([],List2). 		% The first is empty
% sublist([H|T], List2):-
%	search(H, List2),	% We search the head in the second list
%	sublist(T, List2).	% We recursively iterate on the first list tail

% Using dropAny, we cna finally implement sublist the right way
% sublist(X,[5,3,2,1]). -> We can also generate every sublist in this way
sublist([],[]).		% Both are empty
sublist([],List2). 		% The first is empty
sublist([H|T], List2):-
	dropAny(H, List2, NewList),	% We search and drop the occurrence
	sublist(T, NewList).