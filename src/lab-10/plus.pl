% plus(@X, @Y, @Z)
% Performs X plus Y and sets the result in Z
% This approach is fully relational, provided that elements are in Peano's form
% plus(s(zero),s(s(s(zero))),s(s(s(s(zero))))).
plus(X,zero,X). 		% The ! is necessary if we don't want two results for (zero, zero, zero). goal. If the second clause is omitted, the ! can be omitted.
%plus(zero, X, X).  		% Actually not necessary, but it helps finding the solution in less time.
plus(X,s(Y),s(Z)):-plus(X,Y,Z).