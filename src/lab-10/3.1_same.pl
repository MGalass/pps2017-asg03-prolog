% same(List1,List2)
% Checks if two lists contain the same elements, one by one
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).