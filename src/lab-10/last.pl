% last(@List1, @Elem, @List2)
% Used in seqR2. Different parameters combinations lead to different results
% last([1,2,3],5,[1,2,3,5]).
last([], X, [X]).
last([H|T], X, [H1|T1]):-
	H = H1,			% The heads must be equal
	last(T, X, T1).		% Recursion