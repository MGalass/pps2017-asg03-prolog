:-consult('plus.pl').

% I want to show another implementation, trying to make it fully relational
% First we define mul
% mul(@X, @Y, @Z)
% Performs X times Y and sets the result in Z
% This approach is fully relational, provided that elements are in Peano's form
% mul(s(s(s(zero))),s(s(zero)),X). -> X/s(s(s(s(s(s(zero))))))
mul(X,zero,zero).
mul(X,s(Y),Z):-mul(X,Y,A), plus(A,X,Z).