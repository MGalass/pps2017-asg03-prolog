:-consult('mul.pl').

% average(+List,-Average)
% It uses average(+List,-Count,-Sum,-Average)
% Seen in lab
average([],0):-!.				% Added this clause to prevent halting if List is empty
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-
C2 is C+1,
S2 is S+X,
average(Xs,C2,S2,A).

% average_rel(+List,-Average)
% It uses average_rel(+List,-Count,-Sum,-Average)
% This is a fully relation implementation of average(+List, -Average)
% Numbers must be used in Peano's form
% WARNING: Peano's arithmetic is based on integer. We perform a division here and if the result is not an integer, the execution will halt
% Examples:
% average_rel([zero, s(s(zero)), s(zero)],A). -> A/s(zero) [0,2,1] -> 1
% average_rel([s(zero), s(zero), s(zero)],A). -> A/s(zero) [1,1,1] -> 1
% average_rel([s(zero), s(s(s(zero))), s(s(zero))],A). -> A/s(s(zero)) [1,3,2] -> 2
average_rel([],zero):-!.
average_rel(List,A) :- average_rel(List,zero,zero,A).
average_rel([],_,zero,A):- A = zero, !.	% If the sum is zero and List is empty, the average is zero
average_rel([],C,S,A) :- mul(A,C,S), !. % If sum is not zero, then A = S / C. We are using mul to actually perform a division
average_rel([X|Xs],C,S,A) :-
	plus(C,s(zero),C2),
	plus(S,X,S2),
	average_rel(Xs,C2,S2,A).