:-consult('bigger.pl').

% all_bigger(List1,List2)
% Checks if all elements in List1 are bigger than those in List2, one by one
% all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([], []).
all_bigger([X|Xs], [Y|Ys]):- X > Y, all_bigger(Xs, Ys).

% Fully relational version. Numbers must be in Peano's form
% all_bigger_rel([s(s(zero))],[s(zero)]). -> yes
% all_bigger_rel([s(s(zero)),s(s(s(zero))),s(zero)],[s(zero),s(s(zero)),zero]). -> yes ([2,3,1],[1,2,0])
% all_bigger_rel([s(s(zero)), zero],[s(s(zero)), s(zero)]). -> no ([2,0],[2,1])
all_bigger_rel([], []).
all_bigger_rel([X|Xs], [Y|Ys]):- bigger(X,Y,X),!, all_bigger_rel(Xs, Ys).