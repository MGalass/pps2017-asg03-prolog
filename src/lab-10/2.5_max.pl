:-consult('bigger.pl').

% max(+List,-Max)
% Max is the biggest element in List
% Suppose the list has at least one element
% We have to define max(+List,-+Max, -TempMax) first
max(List, M):- max(List, M, 0).
max([],M,TM):- M is TM. % At the end of recursion, we set M equals to TM
max([H|T],M,TM):-	
        H > TM,!,	% If head > temp, we stop searching in other branches
	max(T,M,H).	% We recursively search in the tail
	
max([H|T],M,TM):- max(T,M,TM).	% If head <= temp, we just search in the tail

% max_rel(+List,-Max)
% Max is the biggest element in List. Numbers must be in Peano's form.
% max_rel([zero, s(s(s(s(zero)))), s(zero), s(s(s(zero)))], M). -> M / s(s(s(s(zero)))) [0,4,1,3] -> 4
max_rel(List, M):- max_rel(List, M, zero).	% Starting with zero
max_rel([],M,TM):- M = TM. 
max_rel([H|T],M,TM):-	
        bigger(H,TM,H),!,			% Using bigger predicate
	max_rel(T,M,H).	
	
max_rel([H|T],M,TM):- max_rel(T,M,TM).